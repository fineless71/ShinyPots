package nodomain.shinypots.config;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.resource.language.I18n;

public class ConfigScreen extends Screen {
	private Screen parent = null;

	/* TODO: Remove this once we support not having an empty constructor */
	public ConfigScreen() {}

	public ConfigScreen(Screen parent) {
		this.parent = parent;
	}

	@Override
	public void init() {
		Config.load();

		this.buttons.add(new ButtonWidget(1, this.width / 2 - 100, 10, "Full Slot: " + (Config.fullSlot ? "ON" : "OFF")));
		this.buttons.add(new ButtonWidget(2, this.width / 2 - 100, 40, "Colored: " + (Config.colored ? "ON" : "OFF")));
		this.buttons.add(new ButtonWidget(3, this.width / 2 - 100, 70, "Pots Only: " + (Config.potsOnly ? "ON" : "OFF")));
		this.buttons.add(new ButtonWidget(0, this.width-200-10, this.height-20-10, I18n.translate("gui.done")));
    }

	@Override
	protected void buttonClicked(ButtonWidget button) {
		if (button.id == 1) {
			Config.fullSlot = !Config.fullSlot;
			button.message = "Full Slot: " + (Config.fullSlot ? "ON" : "OFF");
		}

		if (button.id == 2) {
			Config.colored = !Config.colored;
			button.message = "Colored: " + (Config.colored ? "ON" : "OFF");
		}

		if (button.id == 3) {
			/* TODO: Implement pots only */
			//Config.potsOnly = !Config.potsOnly;
			button.message = "Pots Only: " + (Config.potsOnly ? "ON" : "OFF");
		}

		if (button.id == 0) {
			this.client.setScreen(this.parent);
		}
    }

	@Override
    public void render(int mouseX, int mouseY, float tickDelta) {
        this.renderBackground();
        super.render(mouseX, mouseY, tickDelta);
    }

	@Override
	public void removed() {
		Config.save();
    }
}
