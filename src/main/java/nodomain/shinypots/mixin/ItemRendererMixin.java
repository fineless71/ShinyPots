package nodomain.shinypots.mixin;

import java.awt.Color;
import org.lwjgl.opengl.GL11;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import nodomain.shinypots.config.Config;

@Mixin(ItemRenderer.class)
public class ItemRendererMixin {
	@ModifyConstant(method = "method_1543", remap = false)
	private int makeGlintFull(int value) {
		if (Config.fullSlot && value == GL11.GL_DST_ALPHA)
			return GL11.GL_ONE_MINUS_DST_ALPHA;

		return value;
	}

	@Inject(method = "method_6920", remap = false,
	at = @At(value = "INVOKE", target = "Lorg/lwjgl/opengl/GL11;glColor4f(FFFF)V", shift = At.Shift.AFTER))
	private void changeGlintColor(TextRenderer textRenderer, TextureManager textureManager, ItemStack itemStack, int i, int j, CallbackInfo info) {
		if (!Config.colored) return;

		int colorVal = Items.POTION.getDisplayColor(itemStack, 0) | 0xFF000000;
		Color color = new Color(colorVal);
		GL11.glColor4f(color.getRed()/255f, color.getGreen()/255f, color.getBlue()/255f, color.getAlpha()/255f);
	}
}
