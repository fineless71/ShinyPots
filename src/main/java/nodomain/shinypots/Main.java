package nodomain.shinypots;

import net.fabricmc.api.ModInitializer;
import nodomain.shinypots.config.Config;

public class Main implements ModInitializer {
	@Override
	public void onInitialize() {
		Config.load();
	}
}
