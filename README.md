# Shiny Pots

For Legacy Fabric 1.7.x. This mod makes the glint take up a full slot.

## Normal

![Normal screenshot](normal.png)

## Colored

![Colored screenshot](colored.png)

TODO: Option to enchant thrown potions

TODO: Option to make the thrown potion "3D" (2.5D?), like when held in your hand
